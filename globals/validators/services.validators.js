const { obj, string, boolean, array } = require('./validators');

const service = obj.opt({
  name: string.req,
  path: string.opt,
  hasModel: boolean.req,
  hasRoutes: boolean.req
});

exports.servicesSchema = array.req(service);
