const { id, obj, string } = require('./validators');

module.exports = {
  paramsSchema: {
    key: { key: id.req }
  },
  payloadSchema: {
    create: obj.req({
      label: string.req,
      language: string.opt
    })
  }
};
