const Joi = require('joi');
const { servicesSchema } = require('./services.validators');

module.exports = {
  validateServices: services => Joi.validate(services, servicesSchema),
  validatePayload: ({ body }, schema) => Joi.validate(body, schema),
  validateParams: ({ params }, schema) => Joi.validate(params, schema)
};
