const { environnement } = require('../../config/config');

module.exports = {
  getEnvironnement: () => {
    switch (environnement) {
      case 'development':
        return 'DEV';
      case 'production':
      default:
        return 'PROD';
    }
  }
};
