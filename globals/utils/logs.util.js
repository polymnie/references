/* eslint-disable no-console */
const { inspect } = require('util');
const moment = require('moment');

const config =
  process.env.NODE_ENV === 'test'
    ? require('../../config/config.test')
    : require('../../config/config');

const { logError = true, logRoutes = true } = config;

const colors = {
  white: '\x1b[37m',
  red: '\x1b[31m',
  green: '\x1b[32m',
  yellow: '\x1b[33m',
  blue: '\x1b[34m'
};

/*
 * Log info
 * @param {String} message : router name
 * @param {String} color : code color
 */
const infoLogger = (message, color = colors.white) => {
  console.info(color, message);
};
/*
 * Log error
 * @param {Any} error : error message
 * @param {String} color : code color
 */
const errorLogger = (error, color = colors.white) => {
  console.error(color, error);
};
/*
 * Log router status
 * @param {String} name : router name
 * @param {String} status : status router
 */
const logRouter = (name, status, color) => {
  const length = 48;
  const symbol = '-';
  const router = `ROUTER ${status}`;
  const separator = symbol.repeat(length - name.length - router.length);
  const message = `${name.toUpperCase()} ${separator} ${router}`;
  infoLogger(message, color);
};

module.exports = {
  /*
   * Deep Log with color depends type
   * @param {Any} value : data to see in log
   * @param {String} message : optional message
   * @param {Number} index : optional indentation
   */
  dl: (value, message = '', indentation = -1) => {
    const text =
      indentation < 0
        ? `### ${message}`
        : `${'¤¤ '.repeat(indentation + 1)} ${message}`;
    console.log(text, inspect(value, false, null, true));
  },
  /*
   * Conditionnal log
   * @param {Boolean} conditions : display log or not
   * @param {Any} value : data to see in log
   * @param {String} message : optional message
   */
  cond: (conditions, value, message = '** ASSERT ** : ') => {
    console.assert(message, inspect(value, false, null, true));
  },
  /*
   * Log specific to array and object
   * @param {Array|Object} data : data to see in log
   */
  taber: data => {
    console.table(data);
  },
  /*
   * Log the path and type route
   * @param {String} name : name of model
   */
  logRoute: ({ url = '', method = '' }) => {
    if (logRoutes) {
      infoLogger(`${method} : ${url}`);
    }
  },
  /*
   * Log to see error throw in log
   * @param {Any} error : error message
   */
  logErr: error => {
    if (logError) errorLogger(error);
  },
  /*
   * Log when server start
   * @param {String} port : error message
   */
  logStart: port => {
    const time = moment().format('HH:mm:ss');
    infoLogger(`Server is listening on port ${port} -------- ${time}`);
  },
  /*
   * Log when connection to DB is ok
   * @param {String} env : name of current environnement
   * @param {String} db : uri of database
   */
  logDb: (env, db) => {
    infoLogger(`Database ${env} ** ONLINE **`, colors.green);
    infoLogger(`Connected to ${db}`);
  },
  /*
   * Log when connection to DB is failed
   * @param {String} env : name of current environnement
   * @param {String} db : uri of database
   */
  logDbErr: (env, db) => {
    errorLogger(`Database ${env} ** OFFLINE **`, colors.red);
    errorLogger(`Failed to connect on ${db}`, colors.white);
  },
  /*
   * Log the model registered
   * @param {String} name : name of model
   */
  logModel: (servicesCount, modelsRegistered) => {
    if (servicesCount === modelsRegistered) {
      infoLogger('************** MODELS ARE REGISTERED **************');
    } else if (modelsRegistered === 0) {
      errorLogger(
        '**************   ALL MODELS ARE KO   **************',
        colors.red
      );
    } else {
      errorLogger(
        '**************    ERROR IN MODELS    **************',
        colors.yellow
      );
    }
  },
  /*
   * Log the model registered
   * @param {String} name : name of model
   */
  logModelError: name => {
    errorLogger(`!!! warning : model ${name} is unavailable !!!`, colors.red);
  },
  /*
   * Log the routes service created with status ON
   * @param {String} name : name of routes service
   */
  logRoutesOn: name => {
    logRouter(name, 'ON', colors.white);
  },
  /*
   * Log the routes service created with status OFF
   * @param {String} name : name of routes service
   */
  logRoutesOff: name => {
    logRouter(name, 'OFF', colors.red);
  },
  /*
   * Log list routes offline
   * @param {Boolean} routesIsKO : status of routers
   * @param {Array} routes : list name of routes ko
   */
  logStatusRoutes: (routesIsKO = false, routes = []) => {
    if (routesIsKO) {
      const separator = ' - ';
      if (logError)
        errorLogger(
          `!!! warning : routes offline > ${routes.join(
            separator
          )} !!!`.toUpperCase(),
          colors.yellow
        );
    }
  },
  /*
   * Log the path and type route
   * @param {String} name : name of model
   */
  logInvalidService: ({ name = '', details = [], ...e }) => {
    errorLogger(colors.red, `!!! Warning : Error services - ${name}  !!!`);
    if (logError) {
      errorLogger(details);
      errorLogger(e);
    }
  }
};
