const { development = true } = require('../../config/config');
const { logErr } = require('./logs.util');
const { success, clientError, serverError } = require('../constants/httpCodes');
/*
 * Response server for request HTTP, send code 404 with message
 * @param {Object} res : HTTP Express response
 * @param {String} dataName : Name of data to send or error
 */
const responseNotFound = (res, dataName = 'error') => {
  res.status(clientError.notFound).json({ [dataName]: 'Not Found' });
};

module.exports = {
  responseNotFound,
  /*
   * Response server for request HTTP, send code only code 201
   * @param {Object} res : HTTP Express response
   */
  responseCreated: (res, document) => {
    const data = document.toObject();
    delete data.__v;
    delete data.type;
    delete data.updated;
    res.status(success.created).json(data);
  },
  /*
   * Response server for request HTTP, send code 200 with data
   * @param {Any} data : Data to send
   * @param {String} dataName : Name of data to send
   * @param {Object} res : HTTP Express response
   */
  responseGet: (data, dataName, res) => {
    if (data) res.status(success.ok).json(data);
    else responseNotFound(res, dataName);
  },
  /*
   * Response server for request HTTP, send only code 204
   * @param {Object} res : HTTP Express response
   */
  responseNoContent: res => {
    res.status(success.noContent).end();
  },
  /*
   * Response server for request HTTP when router is off, send only code 500
   * @param {Object} res : HTTP Express response
   */
  responseNotAvailable: res => {
    const response = {
      message: 'This service is unavailable ... Try later'
    };
    res.status(serverError.internal).json(response);
  },
  /*
   * Response server for request HTTP, send code and error message
   * Check if validation error, or error with code
   *
   * @param {Object} error : error to send
   * @param {Number} code : http code response
   * @param {Object} res : HTTP Express response
   */
  responseError: (res, error, code = serverError.internal) => {
    let errorMessage = error || 'Error has occurred';
    let httpCode = code;
    logErr(error);
    if (typeof error === 'object' && error.name === 'ValidationError') {
      const { name, details = [] } = error;
      const messages = details.map(({ message }) => {
        return message;
      });
      errorMessage = {
        error: name,
        ...(development && { messages }) // if developement mode, display details validator errors
      };
    } else if (typeof error === 'object' && error.code) {
      httpCode = error.code;
      errorMessage = { error: error.error };
    } else if (typeof error === 'string') {
      errorMessage = { error };
    } else {
      errorMessage = { error: 'Error has occurred' };
    }
    res.status(httpCode).json(errorMessage);
  }
};
