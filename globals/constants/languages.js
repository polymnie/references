const FR = 'fr';
const EN = 'en';

module.exports = Object.freeze({
  FR,
  EN,
  LANGUAGES_ENUM: [FR, EN]
});
