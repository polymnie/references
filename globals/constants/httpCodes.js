module.exports = Object.freeze({
  information: {
    continue: 100
  },
  success: {
    ok: 200,
    created: 201,
    accepted: 202,
    noContent: 204,
    resetContent: 205
  },
  clientError: {
    badRequest: 400,
    unauthorised: 401,
    forbidden: 403,
    notFound: 404,
    conflict: 409
  },
  serverError: {
    internal: 500
  }
});
