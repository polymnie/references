# Polymnie - References Api

References polymnie application is a RESTful api. It allow you to create, read, update or delete leisures, skills or personalities. These entities are saved in References collection.

Discriminator mongoose is used on References model. The discriminator key is *type*, which has in value **Skill**, **Leisure** or **Personality**.

## Configuration
In the folder config, you can find two json files. They are saved with sample extension. You must copy both and remove sample.

- config.json
- config.test.json

| **Fields** | **Types** | **Descriptions** |
| ------ | ------ | ------ |
| environnement | String | Environnement declaration|
| port | Number | Port to start the server |
| development | Boolean | Indicates if development mode is ON or OFF|
| logError | Boolean | Displays logs error if true |
| logRoutes | Boolean | Displays logs route if true |
| db | Object | Contains mongoose connection informations |
| db.uriConnect | String | URL to connect API to mongoDB  |
| db.useNewUrlParser | Boolean | Allow new parser on URL for the connection |
| db.useCreateIndex | Boolean | Alow to use index in Mongoose schemas |
| db.useFindAndModify | Boolean | Allow to use function mongoose like findOneAndUpdate |

:warning: - In db informations, only uriConnect can be changed. Otherwise you will get messages error.

### Environnement
* **production** | **development** : depends of your configuration file
* **test** : to launch tests

### Development mode
When development mode is activated :
- Fixtures route are available; you can generate fixtures or clean Reference collection
- Details information are available on validators messages error

## Installation
1. Git clone the project
2. In a terminal, on the root project : ``npm install``
3. Duplicate config.sample.json and named it config.json
4. Update informations, choose :
    * Environnement name
    * Server port to start
    * Development mode
    * Autorisations log
    * URL of database
5. Duplicate config.test.sample.json and named it config.test.json
6. Update informations : server port to start
7. Update informations, choose :
    * Server port to start
    * URL of database

## Commands
* ``npm start`` - Start the server
* ``npm run dev`` - Start the server with nodemon
* ``npm test`` - Start functional tests with minimum report in test environnement
* ``npm test:all`` - Start functional tests with details report in test environnement
* ``npm run lint`` - Lint the project
* ``npm run lint:fix`` - Start the linter on the project and fix some errors

## Entities
Mongoose is using for the project. There is one model, named Reference, which creates three others models, Leisure, Personality and Skill. Curently, schemas are same.

Language and translates fields are Reference within schema. But they aren't used for the moment. Future feature is to save data with language index.

### Reference schema :

| **Fields** | **Types** | **Descriptions** |
| ------ | ------ | ------ |
| _id | ObjectId | Id of reference document |
| type | String | Discriminator key to identify Reference type |
| created | Date | Date of document creation |
| updated | Date | Date of the last document modification |
| label | String | Label of reference **required** |
| language | String | Language of label **'fr' by default** |
| translate | Array | List of object. Constains translation |
| translate.label | String | Label of reference **required** |
| translate.language | String | Language of label **required** |
| __v | Number | Count document modifications |
	

## Routes
Following routes are available :
### Skills
* ``POST /skills`` - Save a skill
* ``GET /skills/:id`` - Get details about one specific skill
* ``GET /skills`` - Get details about all skills
* ``PATCH /skills/:id`` - Update information on one skill
* ``DELETE /skills/:id`` - Delete one skill
### Personalities
* ``POST /personalities`` - Save a personality
* ``GET /personalities/:id`` - Get details about one specific personality
* ``GET /personalities`` - Get details about all personalities
* ``PATCH /personalities/:id`` - Update information on one personality
* ``DELETE /personalities/:id`` - Delete one personality
### Leisures
* ``POST /leisures`` - Save a leisure
* ``GET /leisures/:id`` - Get details about one specific leisure
* ``GET /leisures`` - Get details about all leisures
* ``PATCH /leisures/:id`` - Update information leisureon one leisure
* ``DELETE /leisures/:id`` - Delete one leisure
### In development mode
* ``POST /fixtures/generate`` - Generate fixtures for each references.
* ``DELETE /fixtures/clear`` - Delete all documents in reference Collection