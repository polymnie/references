const router = require('express').Router();

const FixturesController = require('./fixtures.controller');
const { responseNotFound } = require('../../globals/utils/responses.util');

router
  .post('/generate', (req, res) => {
    FixturesController.generateAction(req, res);
  })
  .delete('/clear', (req, res) => {
    FixturesController.clearAction(req, res);
  })
  .all('*', (req, res) => {
    responseNotFound(res);
  });

module.exports = router;
