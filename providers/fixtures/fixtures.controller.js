// models
const Leisure = require('../../api/models/leisures.model');
const Personality = require('../../api/models/personalities.model');
const Skill = require('../../api/models/skills.model');
// mocks
const { fakeLeisures } = require('../mocks/leisures');
const { fakePersonalities } = require('../mocks/personalities');
const { fakeSkills } = require('../mocks/skills');

// Constants & utils
const { success, serverError } = require('../../globals/constants/httpCodes');
const { logErr } = require('../../globals/utils/logs.util');

const models = [
  {
    Model: Leisure,
    mock: fakeLeisures
  },
  {
    Model: Personality,
    mock: fakePersonalities
  },
  {
    Model: Skill,
    mock: fakeSkills
  }
];
/**
 * Return only error response
 * @params {Array} response : list of mongoose action
 * @return {Array} List of error mongoose
 * */
function buildResponse(response) {
  return response.filter(res => {
    return res.hasOwnProperty('error');
  });
}
/**
 * Generate all reference data
 * @return {Array} Own differents generation results
 * */
async function clearAllCollection() {
  const promises = models.map(({ Model }) => {
    try {
      return Model.deleteMany();
    } catch (error) {
      logErr(error);
      return {
        error: `Clear - Error occured on ${Model.modelName} Collection`
      };
    }
  });
  const response = await Promise.all(promises);
  return buildResponse(response);
}
/**
 * Generate all reference data
 * @return {Array} Own differents generation results
 * */
async function generateAll() {
  const promises = models.map(({ Model, mock }) => {
    try {
      return Model.create(mock);
    } catch (error) {
      logErr(error);
      return {
        error: `Generate - Error occured on ${Model.modelName} Collection`
      };
    }
  });
  const response = await Promise.all(promises);
  return buildResponse(response);
}

module.exports = {
  clearAllCollection,
  generateAll,
  async generateAction(req, res) {
    const responseClear = await clearAllCollection();
    const responseGenerate = await generateAll();
    const errors = [...responseClear, ...responseGenerate].reduce((acc, el) => {
      return [...acc, ...el];
    }, []);
    const codeResponse =
      errors.length === 0 ? success.created : serverError.internal;
    const response =
      errors.length === 0 ? { status: 'ok' } : { status: 'ko', errors };
    res.status(codeResponse).json({ response });
  },
  async clearAction(req, res) {
    const errors = await clearAllCollection();
    const codeResponse =
      errors.length === 0 ? success.created : serverError.internal;
    const response =
      errors.length === 0 ? { status: 'ok' } : { status: 'ko', errors };
    res.status(codeResponse).json({ response });
  }
};
