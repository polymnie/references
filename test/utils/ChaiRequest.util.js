module.exports = class ChaiRequest {
  constructor(api, chai) {
    this.api = api;
    this.chai = chai;
  }

  post(path = '/', payload = {}, response) {
    this.chai
      .request(this.api)
      .post(path)
      .send(payload)
      .end(response);
  }

  get(path = '/', response) {
    this.chai
      .request(this.api)
      .get(path)
      .end(response);
  }

  patch(path = '/', payload = {}, response) {
    this.chai
      .request(this.api)
      .patch(path)
      .send(payload)
      .end(response);
  }

  put(path = '/', payload = {}, response) {
    this.chai
      .request(this.api)
      .put(path)
      .send(payload)
      .end(response);
  }

  delete(path = '/', response) {
    this.chai
      .request(this.api)
      .delete(path)
      .end(response);
  }
};
