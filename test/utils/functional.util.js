const { expect } = require('chai');
const {
  success,
  clientError,
  serverError
} = require('../../globals/constants/httpCodes');

const messages = {
  notFound: 'Not Found',
  validationError: 'ValidationError',
  alreadyExist: 'Already exist'
};

const fieldsPost = ['_id', 'created', 'label', 'language', 'translate'];
const fieldsGet = [
  '_id',
  'created',
  'updated',
  'label',
  'language',
  'translate'
];
/*
 * Check if response has status with good http code and body
 * @params{Object} res : response of api
 * @params{Number} httpCode : http code waiting
 */
const hasBody = (res, httpCode) => {
  expect(res).to.be.an('object');
  expect(res).to.have.property('status');
  expect(res).to.have.property('body');
  expect(res.status).to.be.equal(httpCode);
  return res.body;
};
/*
 * Check if response has status with good http code and body
 * @params{Object} body : body response of api
 * @params{Object} payload : Data send to the api
 * @params{Array} fields : list of fields which be in body
 */
const expectBody = (body, payload, fields) => {
  expect(body).to.be.an('object');
  expect(body).to.have.all.keys(fields);
  expect(body.label).to.equal(payload.label);
  expect(body.language).to.equal('fr');
  expect(body.translate).to.be.an('array');
};
/*
 * Check if response is an error and check the message
 * @params{Object} body : body response of api
 * @params{String} errorMessage : message that should be receive
 */
const hasError = (body, errorMessage) => {
  expect(body).to.have.property('error');
  expect(body.error).to.be.equal(errorMessage);
  if (errorMessage === 'ValidationError') {
    expect(body).to.have.property('messages');
    expect(body.messages).to.be.an('array');
  }
};
/*
 * Check if response has status with good http code and body
 * @params{Object} body : body response of api
 * @params{String} errorMessage : message that should be receive
 * @params{String} label : label of data not found
 */
const notFound = (body, errorMessage, label = 'error') => {
  expect(body).to.have.property(label);
  expect(body[label]).to.be.equal(errorMessage);
};

module.exports = {
  /*
   * Expect for success response
   */
  successExpect: {
    /*
     * Check that is response for document creation
     * @params{Object} res : response of api
     * @params{Object} payload : Data send to the api
     */
    isCreated: (res, payload) => {
      const body = hasBody(res, success.created);
      expectBody(body, payload, fieldsPost);
    },
    /*
     * Check that is response for document edition
     * @params{Object} res : response of api
     * @params{Object} payload : Data send to the api
     */
    isUpdated: (res, payload) => {
      const body = hasBody(res, success.ok);
      expectBody(body, payload, fieldsGet);
      expect(body.updated).to.not.equal(null);
    },
    /*
     * Check that is response for document deletion
     * @params{Object} res : response of api
     */
    isDeleted: res => {
      const body = hasBody(res, success.noContent);
      expect(body).to.be.an('object');
      // eslint-disable-next-line no-unused-expressions
      expect(body).to.be.empty;
    },
    /*
     * Check that is response for one document details
     * @params{Object} res : response of api
     * @params{Object} payload : Data send to the api
     */
    isGot: (res, payload) => {
      const body = hasBody(res, success.ok);
      expectBody(body, payload, fieldsGet);
    },
    /*
     * Check that is response for en empty list
     * @params{Object} res : response of api
     */
    getAllEmpty: res => {
      const body = hasBody(res, success.ok);
      expect(body).to.be.an('array');
      // eslint-disable-next-line no-unused-expressions
      expect(body).to.be.empty;
    },
    /*
     * Check that is response for all documents details
     * @params{Object} res : response of api
     */
    getAll: res => {
      const body = hasBody(res, success.ok);
      expect(body).to.be.an('array');
      body.map(element => {
        return expect(element).to.have.all.keys(fieldsGet);
      });
    }
  },
  /*
   * Expect for error response
   */
  errorExpect: {
    /*
     * Check that response is message not found
     * @params{Object} res : response of api
     */
    notFound: res => {
      const body = hasBody(res, clientError.notFound);
      notFound(body, messages.notFound);
    },
    /*
     * Check that response is data not found
     * @params{Object} res : response of api
     * @params{String} label : label of data not found
     */
    notFoundRessource: (res, label) => {
      const body = hasBody(res, clientError.notFound);
      notFound(body, messages.notFound, label);
    },
    /*
     * Check that is error validator
     * @params{Object} res : response of api
     */
    validatorError: res => {
      const body = hasBody(res, serverError.internal);
      hasError(body, messages.validationError);
      expect(body).to.have.property('messages');
      expect(body.messages).to.be.an('array');
    },
    /*
     * Check that data already exists
     * @params{Object} res : response of api
     */
    alreadyExist: res => {
      const body = hasBody(res, clientError.conflict);
      hasError(body, messages.alreadyExist);
    }
  }
};
