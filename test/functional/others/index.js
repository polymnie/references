const actionPost = require('./badPath.post.test.js');
const actionGet = require('./badPath.get.test.js');
const actionPatch = require('./badPath.patch.test.js');
const actionPut = require('./badPath.put.test.js');
const actionDelete = require('./badPath.delete.test.js');

module.exports = Request => {
  context('Bad path API', () => {
    actionPost(Request);
    actionGet(Request);
    actionPatch(Request);
    actionPut(Request);
    actionDelete(Request);
  });
};
