const { errorExpect } = require('../../utils/functional.util');

module.exports = Request => {
  describe('Post request', () => {
    const path1 = '/';
    it(`Should be error : path '${path1}'`, done => {
      Request.post(path1, {}, (err, res) => {
        errorExpect.notFound(res);
        done();
      });
    });
    const path2 = '/badpath';
    it(`Should be error : path '${path2}'`, done => {
      Request.post(path2, {}, (err, res) => {
        errorExpect.notFound(res);
        done();
      });
    });
    const path3 = '/badpath/bad';
    it(`Should be error : path '${path3}'`, done => {
      Request.post(path3, {}, (err, res) => {
        errorExpect.notFound(res);
        done();
      });
    });
    const path4 = '/skill/bad';
    it(`Should be error : path '${path4}'`, done => {
      Request.post(path4, {}, (err, res) => {
        errorExpect.notFound(res);
        done();
      });
    });
  });
};
