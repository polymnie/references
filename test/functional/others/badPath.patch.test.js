const { errorExpect } = require('../../utils/functional.util');

module.exports = Request => {
  describe('Patch request', () => {
    const path1 = '/';
    it(`Should be error : path '${path1}'`, done => {
      Request.patch(path1, {}, (err, res) => {
        errorExpect.notFound(res);
        done();
      });
    });
    const path2 = '/badpath';
    it(`Should be error : path '${path2}'`, done => {
      Request.patch(path2, {}, (err, res) => {
        errorExpect.notFound(res);
        done();
      });
    });
    const path3 = '/badpath/bad';
    it(`Should be error : path '${path3}'`, done => {
      Request.patch(path3, {}, (err, res) => {
        errorExpect.notFound(res);
        done();
      });
    });
    const path4 = '/skill/bad';
    it(`Should be error : path '${path4}'`, done => {
      Request.patch(path4, {}, (err, res) => {
        errorExpect.notFound(res);
        done();
      });
    });
  });
};
