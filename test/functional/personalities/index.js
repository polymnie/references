const actionPost = require('./personalities.post.test.js');
const actionGet = require('./personalities.get.test.js');
const actionPatch = require('./personalities.patch.test.js');
const actionDelete = require('./personalities.delete.test.js');
const actionBadPatch = require('./personalities.badPath.test.js');

const kind = 'personalities';
const type = 'Personality';
module.exports = Request => {
  context('Personalities API', () => {
    actionGet(Request, kind, type);
    actionPost(Request, kind, type);
    actionPatch(Request, kind, type);
    actionDelete(Request, kind, type);
    actionBadPatch(Request, kind, type);
  });
};
