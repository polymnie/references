const referencesPatch = require('../references/references.patch.test');

module.exports = (Request, kind, type) => {
  describe('Update actions', () => {
    referencesPatch(Request, kind, type);
  });
};
