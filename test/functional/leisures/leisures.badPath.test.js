const referencesBadPath = require('../references/references.badPath.test');

module.exports = (Request, kind) => {
  describe('Try bad path', () => {
    referencesBadPath(Request, kind);
  });
};
