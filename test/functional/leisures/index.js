const actionPost = require('./leisures.post.test.js');
const actionGet = require('./leisures.get.test.js');
const actionPatch = require('./leisures.patch.test.js');
const actionDelete = require('./leisures.delete.test.js');
const actionBadPatch = require('./leisures.badPath.test.js');

const kind = 'leisures';
const type = 'Leisure';
module.exports = Request => {
  context('Leisures API', () => {
    actionGet(Request, kind, type);
    actionPost(Request, kind, type);
    actionPatch(Request, kind, type);
    actionDelete(Request, kind, type);
    actionBadPatch(Request, kind, type);
  });
};
