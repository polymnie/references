const { successExpect, errorExpect } = require(`../../utils/functional.util`);

module.exports = (Request, kind) => {
  it(`Should be success DELETE '/${kind}' :`, done => {
    const payload = { label: `testdelete1` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      const {
        body: { _id }
      } = res;
      Request.delete(`/${kind}/${_id}`, (err2, res2) => {
        successExpect.isDeleted(res2);
        done();
      });
    });
  });

  it(`Should be sucess DELETE '/${kind}' : document doesn't exist and get no content`, done => {
    const payload = { label: `testget4` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      const {
        body: { _id }
      } = res;
      Request.delete(`/${kind}/${_id}`, (err2, res2) => {
        successExpect.isDeleted(res2);
        Request.delete(`/${kind}/${_id}`, (err3, res3) => {
          successExpect.isDeleted(res3);
          done();
        });
      });
    });
  });

  it(`Should be error DELETE '/${kind}' : missing _id`, done => {
    const payload = { label: `testdelete3` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      Request.delete(`/${kind}/`, (err2, res2) => {
        errorExpect.notFound(res2);
        done();
      });
    });
  });

  it(`Should be error DELETE '/${kind}' : _id is not id`, done => {
    const payload = { label: `testdelete4` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      Request.delete(`/${kind}/isnotanid`, (err2, res2) => {
        errorExpect.validatorError(res2);
        done();
      });
    });
  });
};
