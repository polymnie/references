const { successExpect, errorExpect } = require(`../../utils/functional.util`);

module.exports = (Request, kind) => {
  it(`Should be success POST '/${kind}' : create new document : receive code 201 and data`, done => {
    const payload = { label: 'testpost1' };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      done();
    });
  });

  it(`Should be error POST '/${kind}' : is already saved path`, done => {
    const payload = { label: 'testpost2' };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      Request.post(`/${kind}`, payload, (err2, res2) => {
        errorExpect.alreadyExist(res2);
        done();
      });
    });
  });

  it(`Should be error POST '/${kind}' : doesn't accept empty payload`, done => {
    const payloadEmpty = {};
    Request.post(`/${kind}`, payloadEmpty, (err, res) => {
      errorExpect.validatorError(res);
      done();
    });
  });

  it(`Should be error POST '/${kind}' : missing field required `, done => {
    const payload = { fake: 'testpost3' };
    Request.post(`/${kind}`, payload, (err, res) => {
      errorExpect.validatorError(res);
      done();
    });
  });

  it(`Should be error POST '/${kind}' : doesn't allow fields in payload `, done => {
    const payload = {
      label: 'test 3',
      fake: 'test 4',
      wrong: 'test 5'
    };
    Request.post(`/${kind}`, payload, (err, res) => {
      errorExpect.validatorError(res);
      done();
    });
  });
};
