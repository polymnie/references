const { successExpect, errorExpect } = require(`../../utils/functional.util`);

module.exports = (Request, kind, type) => {
  it(`Should be success PATCH '/${kind}' : document is updated`, done => {
    const payload = { label: `testpatch1` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      const {
        body: { _id }
      } = res;
      const payloadUpdated = { label: `testpatch1updated` };
      Request.patch(`/${kind}/${_id}`, payloadUpdated, (err2, res2) => {
        successExpect.isUpdated(res2, payloadUpdated);
        done();
      });
    });
  });

  it(`Should be error PATCH '/${kind}' : missing field required`, done => {
    const payload = { label: `testpatch2` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      const {
        body: { _id }
      } = res;
      const payloadUpdated = {};
      Request.patch(`/${kind}/${_id}`, payloadUpdated, (err2, res2) => {
        errorExpect.validatorError(res2);
        done();
      });
    });
  });

  it(`Should be error PATCH '/${kind}' : is already saved`, done => {
    const payload = { label: `testpatch31` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      const payload2 = { label: 'testpatch32' };
      Request.post(`/${kind}`, payload2, (err2, res2) => {
        successExpect.isCreated(res2, payload2);
        const {
          body: { _id }
        } = res2;
        Request.patch(`/${kind}/${_id}`, payload, (err3, res3) => {
          errorExpect.alreadyExist(res3);
          done();
        });
      });
    });
  });

  it(`Should be error PATCH '/${kind}': doesn't allow fields in payload `, done => {
    const payload = { label: `testpatch4` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      const {
        body: { _id }
      } = res;
      const payloadUpdated = {
        label: `testpatch4updated`,
        fake: `test 4`,
        wrong: `test 5`
      };
      Request.patch(`/${kind}/${_id}`, payloadUpdated, (err2, res2) => {
        errorExpect.validatorError(res2);
        done();
      });
    });
  });

  it(`Should be error PATCH '/${kind}' : missing _id`, done => {
    const payload = { label: `testpatch5` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      Request.patch(`/${kind}/`, {}, (err2, res2) => {
        errorExpect.notFound(res2);
        done();
      });
    });
  });

  it(`Should be error PATCH '/${kind}' : document doesn't exist`, done => {
    const payload = { label: `testpatch61` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      const {
        body: { _id }
      } = res;
      Request.delete(`/${kind}/${_id}`, (err2, res2) => {
        successExpect.isDeleted(res2);
        const payload2 = { label: 'testpatch62' };
        Request.patch(`/${kind}/${_id}`, payload2, (err3, res3) => {
          errorExpect.notFoundRessource(res3, type);
          done();
        });
      });
    });
  });

  it(`Should be error PATCH '/${kind}' : _id is not id`, done => {
    const payload = { label: `test7` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      const payloadUpdated = { label: `test7updated` };
      Request.patch(`/${kind}/isnotanid`, payloadUpdated, (err2, res2) => {
        errorExpect.validatorError(res2);
        done();
      });
    });
  });
};
