const { errorExpect } = require(`../../utils/functional.util`);

module.exports = (Request, kind) => {
  it(`Should be error POST '/${kind}/bad/path' : error not found`, done => {
    Request.post(`/${kind}/bad/path`, {}, (err, res) => {
      errorExpect.notFound(res);
      done();
    });
  });

  it(`Should be error GET '/${kind}/bad/path' : error not found`, done => {
    Request.get(`/${kind}/bad/path`, (err, res) => {
      errorExpect.notFound(res);
      done();
    });
  });

  it(`Should be error PUT '/${kind}/bad/path' : error not found`, done => {
    Request.put(`/${kind}/bad/path`, {}, (err, res) => {
      errorExpect.notFound(res);
      done();
    });
  });

  it(`Should be error PATCH '/${kind}/bad/path' : error not found`, done => {
    Request.patch(`/${kind}/bad/path`, {}, (err, res) => {
      errorExpect.notFound(res);
      done();
    });
  });

  it(`Should be error PATCH '/${kind}/' : error not found`, done => {
    Request.patch(`/${kind}/`, {}, (err, res) => {
      errorExpect.notFound(res);
      done();
    });
  });

  it(`Should be error DELET '/${kind}/bad/path' : error not found`, done => {
    Request.delete(`/${kind}/bad/path`, (err, res) => {
      errorExpect.notFound(res);
      done();
    });
  });
};
