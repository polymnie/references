const { successExpect, errorExpect } = require(`../../utils/functional.util`);

module.exports = (Request, kind, type) => {
  it(`Should be success GET '/${kind}' : get empty list`, done => {
    Request.get(`/${kind}`, (err, res) => {
      successExpect.getAllEmpty(res);
      done();
    });
  });

  it(`Should be success GET '/${kind}' : get one`, done => {
    const payload = { label: `testget1` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      const {
        body: { _id }
      } = res;
      Request.get(`/${kind}/${_id}`, (err2, res2) => {
        successExpect.isGot(res2, payload);
        done();
      });
    });
  });

  it(`Should be success GET '/${kind}' : all list filled`, done => {
    const payload = { label: `testget2` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      Request.get(`/${kind}`, (err2, res2) => {
        successExpect.getAll(res2);
        done();
      });
    });
  });

  it(`Should be success GET '/${kind}/' : all list filled`, done => {
    const payload = { label: `testget3` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      Request.get(`/${kind}/`, (err2, res2) => {
        successExpect.getAll(res2);
        done();
      });
    });
  });

  it(`Should be error GET '/${kind}' : document doesn't exist`, done => {
    const payload = { label: `testget4` };
    Request.post(`/${kind}`, payload, (err, res) => {
      successExpect.isCreated(res, payload);
      const {
        body: { _id }
      } = res;
      Request.delete(`/${kind}/${_id}`, (err2, res2) => {
        successExpect.isDeleted(res2);
        Request.get(`/${kind}/${_id}`, (err3, res3) => {
          errorExpect.notFoundRessource(res3, type);
          done();
        });
      });
    });
  });

  it(`Should be error GET '/${kind}' : _id is not id`, done => {
    Request.get(`/${kind}/isnotanid`, (err, res) => {
      errorExpect.validatorError(res);
      done();
    });
  });
};
