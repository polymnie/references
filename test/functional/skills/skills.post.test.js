const referencesPost = require('../references/references.post.test');

module.exports = (Request, kind) => {
  describe('Create actions', () => {
    referencesPost(Request, kind);
  });
};
