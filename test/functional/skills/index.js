const actionPost = require('./skills.post.test.js');
const actionGet = require('./skills.get.test.js');
const actionPatch = require('./skills.patch.test.js');
const actionDelete = require('./skills.delete.test.js');
const actionBadPatch = require('./skills.badPath.test.js');

const kind = 'skills';
const type = 'Skill';
module.exports = Request => {
  context('Skills API', () => {
    actionGet(Request, kind, type);
    actionPost(Request, kind, type);
    actionPatch(Request, kind, type);
    actionDelete(Request, kind, type);
    actionBadPatch(Request, kind, type);
  });
};
