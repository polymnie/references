const { connection } = require('mongoose');
const chai = require('chai');
const chaiHttp = require('chai-http');
const api = require('../../api');
const ChaiRequest = require('../utils/ChaiRequest.util');

chai.use(chaiHttp);
const Request = new ChaiRequest(api, chai);

// LOAD TESTS
const othersTests = require('./others');
const leisuresTests = require('./leisures');
const personalitiesTests = require('./personalities');
const skillsTests = require('./skills');

context('End to end test API', () => {
  before(async () => {
    if (!(connection._readyState === 1 || connection._readyState === 2)) {
      throw new Error('Database offline');
    } else {
      // eslint-disable-next-line global-require
      await require('../../providers/fixtures/fixtures.controller').clearAllCollection();
    }
  });
  after(async () => {
    // eslint-disable-next-line global-require
    await require('../../providers/fixtures/fixtures.controller').clearAllCollection();
  });
  // Clean database before and after
  othersTests(Request);
  leisuresTests(Request);
  personalitiesTests(Request);
  skillsTests(Request);
});
