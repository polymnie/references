// Modules & Middlewares
const express = require('express');

const api = express();
const bodyParser = require('body-parser');

const { port = 8080, ...config } =
  process.env.NODE_ENV === 'test'
    ? require('./config/config.test')
    : require('./config/config');
const { logStart } = require('./globals/utils/logs.util');

// Web server configuration
api.use(bodyParser.json());
api.use(bodyParser.urlencoded({ extended: true }));
require('./init')(api, config);

// Listen server
api.listen(port, () => {
  logStart(port);
});
module.exports = api;
