/*
eslint-disable
  global-require,
  import/no-dynamic-require,
  array-callback-return
*/
const {
  logRoutesOn,
  logRoutesOff,
  logStatusRoutes,
  logRoute,
  logErr
} = require('../globals/utils/logs.util');
const {
  responseNotFound,
  responseNotAvailable
} = require('../globals/utils/responses.util');

module.exports = (api, services, config = {}) => {
  const { development = true } = config;
  let routesIsKO = false;
  const routesKo = [];
  /**
   * Catch error, set status routes in ko, log routes off and push in list route ko to display
   *
   * @param {String} name : route name in error
   * @param {Error} error : error catch
   * */
  const manageError = (path, name, error) => {
    routesIsKO = true;
    routesKo.push(name);
    logRoutesOff(name);
    if (development) logErr(error);
    api.all(path, (req, res) => {
      responseNotAvailable(res);
    });
  };
  // Middleware to log each url route request
  api.use('/', (req, res, next) => {
    logRoute(req);
    next();
  });
  // Use fixtures controller in development mode
  if (development) {
    try {
      api.use('/fixtures', require('../providers/fixtures/fixtures.routes'));
      logRoutesOn('fixtures');
    } catch (error) {
      manageError('/fixtures', 'fixtures', error);
    }
  }
  // Browse all service and add router if they are
  services
    .filter(({ hasRoutes } = false) => hasRoutes)
    .map(({ name, path }) => {
      try {
        // use the router
        api.use(path, require(`../api/routes/${name}.routes`));
        logRoutesOn(name);
      } catch (error) {
        manageError(path, name, error);
      }
    });
  logStatusRoutes(routesIsKO, routesKo); // Liste all routes offline
  api.all('*', (req, res) => {
    responseNotFound(res);
  });
};
