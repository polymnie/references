/*
eslint-disable
global-require,
import/no-dynamic-require,
array-callback-return,
func-names
*/
const mongoose = require('mongoose');
const { logDb, logDbErr } = require('../globals/utils/logs.util');

module.exports = config => {
  const { environnement = 'development', db = {} } = config;
  const mongooseOptions = {
    useNewUrlParser: db.useNewUrlParser || true,
    useCreateIndex: db.useCreateIndex || true,
    useFindAndModify: db.useFindAndModify || false
  };
  // DB Connexion
  (async function({ uriConnect = 'mongodb://127.0.0.1:27017/random' }) {
    try {
      await mongoose.connect(uriConnect, mongooseOptions);
      logDb(environnement, uriConnect);
    } catch (error) {
      logDbErr(environnement, uriConnect);
    }
  })(db);
};
