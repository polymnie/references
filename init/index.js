/*
eslint-disable
  global-require,
  no-multi-assign
*/
const { validateServices } = require('../api/validators/validate');
const services = require('../config/services');
const { logInvalidService } = require('../globals/utils/logs.util');

module.exports = async (api, config) => {
  (async () => {
    try {
      await validateServices(services);
    } catch (error) {
      logInvalidService(error);
      process.exit(0);
    }
    await require('./databaseConnection')(config);
    require('./modelsRegistration')(services);
    require('./router')(api, services, config);
  })();
};
