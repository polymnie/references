/*
eslint-disable
global-require,
import/no-dynamic-require,
array-callback-return,
func-names
*/
const { logModel, logModelError } = require('../globals/utils/logs.util');

let modelsRegistered = 0;

module.exports = services => {
  const servicesWithModel = services.filter(({ hasModel } = false) => hasModel);
  servicesWithModel.map(({ name }) => {
    try {
      require(`../api/models/${name}.model`);
      modelsRegistered += 1;
    } catch (e) {
      logModelError(name);
    }
  });
  logModel(servicesWithModel.length, modelsRegistered);
};
