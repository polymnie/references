/*
 * Contains list of services
 * {String} name : required : name of service
 * {String} path : optional: path route
 * {Boolean} hasRoutes : required : allow options in register
 * {Boolean} hasModel : required : declare if model exist to init it
 */
module.exports = [
  {
    name: 'leisures',
    path: '/leisures',
    hasModel: false,
    hasRoutes: true
  },
  {
    name: 'personalities',
    path: '/personalities',
    hasModel: false,
    hasRoutes: true
  },
  {
    name: 'references',
    hasModel: true,
    hasRoutes: false
  },
  {
    name: 'skills',
    path: '/skills',
    hasModel: false,
    hasRoutes: true
  }
];
