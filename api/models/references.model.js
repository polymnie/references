const mongoose = require('mongoose');

const { Schema } = mongoose;

const { FR, LANGUAGES_ENUM } = require('../../globals/constants/languages');

const options = { discriminatorKey: 'type' };

const referenceSchema = new Schema(
  {
    label: {
      type: String,
      required: [true, "Label can't be blank !"]
    },
    language: {
      type: String,
      lowercase: true,
      index: true,
      enum: LANGUAGES_ENUM,
      default: FR
    },
    translate: [
      {
        label: {
          type: String,
          lowercase: true,
          required: [true, "Label can't be blank !"]
        },
        language: {
          type: String,
          lowercase: true,
          index: true,
          enum: LANGUAGES_ENUM,
          required: [true, 'Need a language !']
        }
      }
    ],
    created: {
      type: Date,
      default: Date.now
    },
    updated: {
      type: Date,
      default: null
    }
  },
  options
);

// eslint-disable-next-line func-names
referenceSchema.pre('findOneAndUpdate', function() {
  this.update({}, { $set: { updated: new Date() } });
});

mongoose.model('Reference', referenceSchema);
