const { Schema, model } = require('mongoose');

const Reference = model('Reference');

module.exports = Reference.discriminator('Leisure', new Schema({}));
