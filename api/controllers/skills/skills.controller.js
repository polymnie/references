const Skill = require('../../models/skills.model');
const GenericRepository = require('../genericRepository.js');
const ApiController = require('../api.controller');

const SkillRepository = new GenericRepository(Skill);

class SkillController extends ApiController {
  constructor() {
    super(SkillRepository);
  }
}

module.exports = new SkillController();
