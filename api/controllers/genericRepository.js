const initOptions = {
  new: true,
  upsert: false,
  runValidators: true
};

module.exports = class GenericRepository {
  /**
   * Build the model repository depends of mongoose model
   *
   * @param {Model} Model : mongoose Model
   * @param {String} key : label of id model. It's '_id' by default
   * */
  constructor(Model, key = '_id') {
    this.Model = Model;
    this.key = key;
  }

  /**
   * Create document(s)
   *
   * @param {Array} data : data to create document
   * @return {Promise} Return promise of mongo database response
   * */
  create(data) {
    return this.Model.create(data);
  }

  /**
   * Get information of a document by its id
   *
   * @param {String} key : id of the document
   * @return {Promise} Return promise of mongo database response
   * */

  getOneById(key, projections = {}) {
    return this.Model.findById(key, projections);
  }

  /**
   * Get information of a document by its id
   *
   * @param {String} key : id of the document
   * @return {Promise} Return promise of mongo database response
   * */
  getOne(conditions = {}, projections = {}) {
    return this.Model.findOne(conditions, projections);
  }

  /**
   * Get all documents of the model
   * @return {Promise} Return promise of mongo database response
   * */
  getAll(projections = {}) {
    return this.Model.find({}, projections);
  }

  /**
   * Get specific informations of documents, depends of theirs conditions and projections
   *
   * @param {Object} conditions : condition to select documents
   * @param {Object} projections : field to include or exclude
   * @return {Promise} Return promise of mongo database response
   * */
  getList(conditions = {}, projections = {}) {
    return this.Model.find(conditions, projections);
  }

  /**
   * Get one document by its id and update fields
   *
   * @param {String} key : id of the document
   * @param {Object} data : field to update
   * @param {Object} optionsUpdate : Contains list options for the query (like select)
   * @return {Promise} Return promise of mongo database response
   * */
  updateOneById(key, data, optionsUpdate = {}) {
    const options = {
      ...optionsUpdate,
      ...initOptions
    };
    return this.Model.findByIdAndUpdate(key, data, options);
  }

  /**
   * Update fields specific documents depends of theirs condition
   *
   * @param {Object} conditions : condition to select documents
   * @param {Object} data : field to update
   * @param {Object} optionsUpdate : Contains list options for the query (like select)
   * @return {Promise} Return promise of mongo database response
   * */
  updateList(conditions, data, optionsUpdate = {}) {
    const options = {
      ...optionsUpdate,
      ...initOptions
    };
    return this.Model.updateMany(conditions, data, options);
  }

  /**
   * Delete one document by its id
   *
   * @param {String} key : id of the document
   * @return {Promise} Return promise of mongo database response
   * */
  deleteOneById(key) {
    return this.Model.findByIdAndRemove(key);
  }

  /**
   * Delete specific documents depends of theirs condition
   *
   * @param {Object} conditions : condition to select documents
   * @return {Promise} Return promise of mongo database response
   * */
  deleteList(conditions) {
    return this.Model.deleteMany(conditions);
  }
};
