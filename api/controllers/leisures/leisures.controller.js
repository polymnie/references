const Leisure = require('../../models/leisures.model');
const GenericRepository = require('../genericRepository.js');
const ApiController = require('../api.controller');

const LeisureRepository = new GenericRepository(Leisure);

class LeisureController extends ApiController {
  constructor() {
    super(LeisureRepository);
  }
}

module.exports = new LeisureController();
