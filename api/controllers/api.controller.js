const {
  responseCreated,
  responseNoContent,
  responseGet,
  responseError
} = require('../../globals/utils/responses.util');

const { clientError } = require('../../globals/constants/httpCodes');

const { validatePayload, validateParams } = require('../validators/validate');
const {
  paramsSchema,
  payloadSchema
} = require('../validators/references.validator');

module.exports = class ApiController {
  constructor(Repository) {
    this.Repository = Repository;
    this.type = Repository.Model.modelName;
  }

  /**
   * Check payload, check if document exist and create it if it's okay
   * */
  async create(req, res) {
    try {
      await validatePayload(req, payloadSchema.create);
      const { body } = req;
      await this.checkIfExist(body);
      const newDocument = await this.Repository.create(body);
      responseCreated(res, newDocument);
    } catch (error) {
      responseError(res, error);
    }
  }

  /**
   * Check params and get document
   * */
  async read(req, res) {
    try {
      await validateParams(req, paramsSchema.key);
      const {
        params: { key = '' }
      } = req;
      const projections = {
        type: 0,
        __v: 0
      };
      const oneDocument = await this.Repository.getOneById(key, projections);
      responseGet(oneDocument, this.type, res);
    } catch (error) {
      responseError(res, error);
    }
  }

  /**
   * Get list of document
   * */
  async list(req, res) {
    try {
      const projections = {
        type: 0,
        __v: 0
      };
      const data = await this.Repository.getAll(projections);
      responseGet(data, this.type, res);
    } catch (error) {
      responseError(res, error);
    }
  }

  /**
   * Check params, check payload, check if label exist and update one document
   * */
  async updateOne(req, res) {
    try {
      await validateParams(req, paramsSchema.key);
      await validatePayload(req, payloadSchema.create);
      const {
        params: { key = '' },
        body
      } = req;
      await this.checkIfExist(body);
      const options = { select: { type: 0, __v: 0 } };
      const oneDocument = await this.Repository.updateOneById(
        key,
        body,
        options
      );
      responseGet(oneDocument, this.type, res);
    } catch (error) {
      responseError(res, error);
    }
  }

  /**
   * Check params and delete document
   * */
  async deleteOne(req, res) {
    try {
      await validateParams(req, paramsSchema.key);
      const {
        params: { key = '' }
      } = req;
      await this.Repository.deleteOneById(key);
      responseNoContent(res);
    } catch (error) {
      responseError(res, error);
    }
  }

  /**
   * Get document by label, throw error if exist
   * */
  async checkIfExist({ label }) {
    const conditions = { label };
    const projections = { _id: 1 };
    const reference = await this.Repository.getOne(conditions, projections);
    if (reference) {
      const errorMessage = {
        code: clientError.conflict,
        error: 'Already exist'
      };
      throw errorMessage;
    }
  }
};
