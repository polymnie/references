const Personality = require('../../models/personalities.model');
const GenericRepository = require('../genericRepository.js');
const ApiController = require('../api.controller');

const PersonalityRepository = new GenericRepository(Personality);

class PersonalityController extends ApiController {
  constructor() {
    super(PersonalityRepository);
  }
}

module.exports = new PersonalityController();
