const Joi = require('joi');

const objectIdLength = 24;
/** *********************************************************************
 ***************************         ID        **************************
 ********************************************************************** */
exports.id = {
  opt: Joi.string()
    .hex()
    .length(objectIdLength)
    .optional(),
  optAllowNull: Joi.string()
    .hex()
    .allow(null)
    .length(objectIdLength)
    .optional(),
  req: Joi.string()
    .hex()
    .length(objectIdLength)
    .required(),
  reqAllowNull: Joi.string()
    .hex()
    .allow(null)
    .length(objectIdLength)
    .required()
};
/** *********************************************************************
 ***************************     TYPE OBJECT    **************************
 ********************************************************************** */
exports.obj = {
  opt: keys =>
    Joi.object()
      .keys(keys)
      .optional(),
  xorOpt: (keys, xor = Object.keys(keys)) =>
    Joi.object()
      .keys(keys)
      .xor(xor)
      .optional(),
  req: keys =>
    Joi.object()
      .keys(keys)
      .required(),
  xorReq: (keys, xor = Object.keys(keys)) =>
    Joi.object()
      .keys(keys)
      .xor(xor)
      .required()
};
/** *********************************************************************
 ***************************     TYPE STRING    **************************
 ********************************************************************** */
exports.string = {
  // All strings with option trim() to get rid whitespace before and after
  opt: Joi.string()
    .trim()
    .optional(),
  optAllowEmpty: Joi.string()
    .trim()
    .allow('')
    .optional(),
  optValid: value =>
    Joi.string()
      .valid(value)
      .optional(),
  req: Joi.string()
    .trim()
    .required(),
  reqAllowEmpty: Joi.string()
    .trim()
    .allow('')
    .required(),
  reqValid: value =>
    Joi.string()
      .valid(value)
      .required()
};
/** *********************************************************************
 ***************************     TYPE NUMBER    **************************
 ********************************************************************** */
exports.number = {
  opt: Joi.number().optional(),
  optMin: (min = 0) =>
    Joi.number()
      .min(min)
      .optional(),
  optMinMax: (min = 0, max = 99) =>
    Joi.number()
      .min(min)
      .max(max)
      .optional(),
  optValid: value =>
    Joi.number()
      .valid(value)
      .optional(),
  req: Joi.number().required(),
  reqMin: (min = 0) =>
    Joi.number()
      .min(min)
      .required(),
  reqMinMax: (min = 0, max = 99) =>
    Joi.number()
      .min(min)
      .max(max)
      .required(),
  reqValid: value =>
    Joi.number()
      .valid(value)
      .required()
};
/** *********************************************************************
 ***************************     TYPE DATE    **************************
 ********************************************************************** */
exports.date = {
  isoOpt: Joi.date()
    .iso()
    .optional(),
  isoOptAllowNull: Joi.date()
    .iso()
    .allow(null)
    .optional(),
  isoReq: Joi.date()
    .iso()
    .required(),
  isoReqAllowNull: Joi.date()
    .iso()
    .allow(null)
    .required()
};
/** *********************************************************************
 ***************************     TYPE BOOLEAN    **************************
 ********************************************************************** */
exports.boolean = {
  opt: Joi.boolean().optional(),
  req: Joi.boolean().required()
};
/** *********************************************************************
 ***************************     TYPE ARRAY    **************************
 ********************************************************************** */
exports.array = {
  opt: item =>
    Joi.array()
      .items(item)
      .optional(),
  optMin: (item, min = 0) =>
    Joi.array()
      .items(item)
      .min(min)
      .optional(),
  optMax: (item, max = 99) =>
    Joi.array()
      .items(item)
      .max(max)
      .optional(),
  optMinMax: (item, min = 0, max = 99) =>
    Joi.array()
      .items(item)
      .min(min)
      .max(max)
      .optional(),
  req: item =>
    Joi.array()
      .items(item)
      .required(),
  reqMin: (item, min = 0) =>
    Joi.array()
      .items(item)
      .min(min)
      .required(),
  reqMax: (item, max = 99) =>
    Joi.array()
      .items(item)
      .max(max)
      .required(),
  reqMinMax: (item, min = 0, max = 99) =>
    Joi.array()
      .items(item)
      .min(min)
      .max(max)
      .required(),
  reqRandom: Joi.array().required()
};
