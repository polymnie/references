const router = require('express').Router();

const PersonalitiesController = require('../controllers/personalities/personalities.controller');
const { responseNotFound } = require('../../globals/utils/responses.util');

router
  .post('/', (req, res) => {
    PersonalitiesController.create(req, res);
  })
  .get('/', (req, res) => {
    PersonalitiesController.list(req, res);
  })
  .get('/:key', (req, res) => {
    PersonalitiesController.read(req, res);
  })
  .patch('/:key', (req, res) => {
    PersonalitiesController.updateOne(req, res);
  })
  .delete('/:key', (req, res) => {
    PersonalitiesController.deleteOne(req, res);
  })
  .all('*', (req, res) => {
    responseNotFound(res);
  });

module.exports = router;
