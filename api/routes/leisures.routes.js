const router = require('express').Router();

const LeisuresController = require('../controllers/leisures/leisures.controller');
const { responseNotFound } = require('../../globals/utils/responses.util');

router
  .post('/', (req, res) => {
    LeisuresController.create(req, res);
  })
  .get('/', (req, res) => {
    LeisuresController.list(req, res);
  })
  .get('/:key', (req, res) => {
    LeisuresController.read(req, res);
  })
  .patch('/:key', (req, res) => {
    LeisuresController.updateOne(req, res);
  })
  .delete('/:key', (req, res) => {
    LeisuresController.deleteOne(req, res);
  })
  .all('*', (req, res) => {
    responseNotFound(res);
  });

module.exports = router;
