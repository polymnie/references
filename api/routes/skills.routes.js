const router = require('express').Router();

const SkillsController = require('../controllers/skills/skills.controller');
const { responseNotFound } = require('../../globals/utils/responses.util');

router
  .post('/', (req, res) => {
    SkillsController.create(req, res);
  })
  .get('/', (req, res) => {
    SkillsController.list(req, res);
  })
  .get('/:key', (req, res) => {
    SkillsController.read(req, res);
  })
  .patch('/:key', (req, res) => {
    SkillsController.updateOne(req, res);
  })
  .delete('/:key', (req, res) => {
    SkillsController.deleteOne(req, res);
  })
  .all('*', (req, res) => {
    responseNotFound(res);
  });

module.exports = router;
